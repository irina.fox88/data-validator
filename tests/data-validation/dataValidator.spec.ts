import {
  DataValidator,
  Schema,
  ValidationResult,
} from '../../data-validation';

describe('DataValidator', () => {
  it('should validate a valid object', () => {
    const schema: Schema = {
      name: { type: 'string', required: true },
      age: { type: 'number', required: true },
    };

    const validator = new DataValidator(schema);

    const validObject = {
      name: 'John Doe',
      age: 25,
    };

    const result: ValidationResult = validator.validate(validObject);

    expect(result.isValid).toBe(true);
  });

  it('should detect missing required fields', () => {
    const schema: Schema = {
      name: { type: 'string', required: true },
      age: { type: 'number', required: true },
    };

    const validator = new DataValidator(schema);

    const invalidObject = {
      name: 'John Doe',
    };

    const result: ValidationResult = validator.validate(invalidObject);

    expect(result.isValid).toBe(false);
    expect(result.errors).toContain('age is required');
  });

  // Test case 3: Incorrect data type
  it('should detect incorrect data types', () => {
    const schema: Schema = {
      age: { type: 'number', required: true },
    };

    const validator = new DataValidator(schema);

    const invalidObject = {
      age: '25',
    };

    const result: ValidationResult = validator.validate(invalidObject);

    expect(result.isValid).toBe(false);
    expect(result.errors).toContain('age must be of type number');
  });

  it('should apply custom validation function', () => {
    const schema: Schema = {
      email: {
        type: 'string',
        validate: (value) => {
          if (!value.includes('@')) {
            return 'must be of type string';
          }
          return true;
        },
      },
    };

    const validator = new DataValidator(schema);

    const validObject = {
      email: 'john.doe@example.com',
    };

    const invalidObject = {
      email: 'invalid-email',
    };

    const validResult: ValidationResult = validator.validate(validObject);
    const invalidResult: ValidationResult = validator.validate(invalidObject);

    expect(validResult.isValid).toBe(true);
    expect(invalidResult.isValid).toBe(false);
    expect(invalidResult.errors).toContain('email must be of type string');
  });
});
