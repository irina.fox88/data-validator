import { ValidationConfig } from "../dataValidator";

export interface Schema {
    [field: string]: ValidationConfig;
}