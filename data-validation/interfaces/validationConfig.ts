export interface ValidationConfig {
    type: 'string' | 'number';
    required?: boolean;
    validate?: (value: any) => boolean | string; // Custom validation function
}