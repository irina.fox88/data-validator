import * as Interfaces from './interfaces';

export class DataValidator {
  private schema: Interfaces.Schema;

  constructor(schema: Interfaces.Schema) {
    this.schema = schema;
  }

  validate(obj: Record<string, any>): Interfaces.ValidationResult {
    const errors: string[] = [];

    for (const field in this.schema) {
      const fieldValue = obj[field];
      const fieldConfig = this.schema[field];

      // Check if the field is required
      if (fieldConfig.required && (fieldValue === undefined || fieldValue === null)) {
        errors.push(`${field} is required`);
      }

      // Check data type
      if (
          fieldValue !== undefined
          && fieldValue !== null
          && typeof fieldValue !== fieldConfig.type
      ) {
        errors.push(`${field} must be of type ${fieldConfig.type}`);
      }

      // Custom validation function
      if (fieldConfig.validate && typeof fieldConfig.validate === 'function') {
        const customValidationResult = fieldConfig.validate(fieldValue);
        if (customValidationResult !== true) {
          errors.push(`${field} ${customValidationResult}`);
        }
      }
    }

    return errors.length === 0 ? { isValid: true } : { isValid: false, errors };
  }
}
