import { DataValidator } from "./dataValidator";
import { Schema, ValidationResult, ValidationConfig } from './interfaces';

export {
  DataValidator,
  Schema,
  ValidationResult,
  ValidationConfig,
};
