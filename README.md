# Description
This module enables users to define data schemas and validate objects against these schemas.

# Running
```
npm install
```

# Usage
## Define Data Schema
Create a data schema using an object with keys representing fields and values representing the validation configuration:

```typescript
import { Schema } from 'data-validation-module';

const userSchema: Schema = { 
  name: { type: 'string', required: true }, 
  age: { type: 'number', required: true }, 
  email: { 
    type: 'string', 
    validate: (value) => value.includes('@') 
  },
};
```

## Create Validator
Instantiate a validator by passing the data schema:

```typescript
import { DataValidator } from 'data-validation-module';
const userValidator = new DataValidator(userSchema);
```

## Validate Object
Use the validate method to check an object against the schema:

```typescript
const userObject = { 
  name: 'John Doe', 
  age: 25, 
  email: 'john.doe@example.com', 
};
const validationResult = userValidator.validate(userObject);

if (validationResult.isValid) { 
  console.log('Object is valid'); 
} else { 
  console.log('Validation errors:', validationResult.errors); 
}
```

## Custom validation
Add ability to create custom validation rule, which will return own error message.

```typescript
const schema: Schema = {
  email: {
    type: 'string',
    validate: (value) => {
      if (!value.includes('@')) {
        return 'must be of type string';
      }
      return true;
    },
  },
};

const validator = new DataValidator(schema);

const validObject = {
  email: 'john.doe@example.com',
};

const validResult: ValidationResult = validator.validate({
  email: 'john.doe@example.com',
}); // valid
const invalidResult: ValidationResult = validator.validate({
    email: 'john.doe',
}); // invalid

```